var canvas = document.querySelector('canvas');
var c = canvas.getContext('2d');

var width = canvas.width = window.innerWidth;
var height = canvas.height = window.innerHeight;

var mouse = {
	x: undefined,
	y: undefined
}

var mr = 50;
var maxRadius = 150;

var colorArray = [
  '#ffaa22',
  '#99ffaa',
  '#00ff00',
  '#4411aa',
  '#ff1100',
  '#F0FE7B',
  '#08CDE4',
  '#76234E'
];

function Circle(x, y, dx, dy, radius) {
	this.x = x;
	this.y = y;
	this.dx = dx;
	this.dy = dy;
	this.radius = radius;
	this.minRadius = radius;
	this.color = colorArray[Math.floor(Math.random() * colorArray.length)];

	// draw circle
	this.draw = function() {
		c.beginPath();
		c.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
		c.fillStyle = this.color;
		c.fill();
	}

	// update ball position
	this.update = function() {
		if(this.x + this.radius > width
			|| this.x - this.radius < 0) {
			this.dx = -this.dx;
		}

		if(this.y + this.radius > height
			|| this.y - this.radius < 0) {
			this.dy = -this.dy;
		}

		this.x += this.dx;
		this.y += this.dy;

		this.draw();
	}

}

var circleArr = [];

// init
function init() {

	circleArr = [];
	
	for(var i = 0; i < 200; i++) {
		var radius = Math.random() * 15 + 1;

    	var x = Math.random() * (width - radius * 2) + radius;
    	var y = Math.random() * (height - radius * 2) + radius;

    	var dx= (Math.random() - 0.5);
    	var dy= (Math.random() - 0.5);

    	circleArr.push(new Circle(x, y, dx, dy, radius));
	}

	animate();
}
// .end of init

// animate
function animate() {
	c.clearRect(0, 0, width, height);

	for(var i = 0; i < circleArr.length; i++) {
		circleArr[i].update();
	}

	requestAnimationFrame(animate);
}
// .animate

init();

window.addEventListener('mousemove', function(e) {
	mouse.x = e.clientX;
	mouse.y = e.clientY;
});

window.addEventListener('resize', function() {
	width = canvas.width = window.innerWidth;
	height = canvas.height = window.innerHeight;

	init();
});