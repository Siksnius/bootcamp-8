<?php
	
	require('db/connection.php');

	// Duomenu spausdinimas is DB
	$stmt = $conn->prepare("SELECT * FROM prekes");
	$stmt->execute();

	$result = $stmt->fetchAll();

	// Spausdinimas duomenu
	// foreach($result as $row) {
	// 	echo $row['vaisius'] . " " . $row['kiekis'] . " " . $row['kaina'] . "<br>";
	// }

	$conn = null;
?>





<!DOCTYPE html>
<html>
<head>
	<title>Vaisiai - nuo ju skauda galva</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<h1>Vaisiai:</h1>

		<a href="insertFruit.php" class="btn btn-success">
			Įterpti naują vaisių
		</a>

		<table class="table table-hover">
			<tr>
				<th>Pavadinimas</th>
				<th>Kiekis, kg</th>
				<th>Kaina, €</th>
				<th>Veiksmas</th>
			</tr>
			<?php
				// foreach($result as $row) {
				// 	echo "<tr>";
				// 	echo "<td>" . $row['vaisius'] . "</td>";
				// 	echo "<td>" . $row['kiekis'] . "</td>";
				// 	echo "<td>" . $row['kaina'] . "</td>";
				// 	echo "</tr>";
				// }
				
				foreach ($result as $row) :?>
					<tr>
						<td><?php echo $row['vaisius']; ?></td>
						<td><?php echo $row['kiekis']; ?></td>
						<td><?php echo $row['kaina']; ?></td>
						<td>
							<a href="updateFruit.php?num=<?php echo $row['id']; ?>" 
								class="btn btn-warning">
								Atnaujinti
							</a>
							<a href="db/delete.php?num=<?php echo $row['id']; ?>" 
								class="btn btn-danger">
								Trinti
							</a>
						</td>
					</tr>
				<?php
				endforeach;
			?>
		</table>
	</div>
</body>
</html>


	


